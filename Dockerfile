FROM docker:24.0.4-git

RUN apk add -q --upgrade --no-cache --virtual .deps \
        bash \
        curl \
        jq \
        wget\
    && \
    # Clean up
    rm -rf /var/cache/apk/*
